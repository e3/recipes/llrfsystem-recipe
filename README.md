# llrfsystem conda recipe

Home: "https://gitlab.esss.lu.se/e3/rf/e3-llrfsystem/llrfsystem"

Package license: BSD 3-Clause

Recipe license: BSD 3-Clause

Summary: EPICS llrfsystem module
