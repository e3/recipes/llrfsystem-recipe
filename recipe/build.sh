#!/bin/bash

echo $E3_MODULE_SRC_PATH

LIBVERSION=${PKG_VERSION}
E3_MODULE_SRC_PATH=llrfsystem-loc

# Clean between variants builds
make -f ../Makefile.E3 -C ${E3_MODULE_SRC_PATH} clean

make -f ../Makefile.E3 -C ${E3_MODULE_SRC_PATH} MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION}
make -f ../Makefile.E3 -C ${E3_MODULE_SRC_PATH} MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} db
make -f ../Makefile.E3 -C ${E3_MODULE_SRC_PATH} MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} install

